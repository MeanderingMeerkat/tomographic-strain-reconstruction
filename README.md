# tomographic-strain-reconstruction

A library for reconstructing tensor fields using tomography.

At this stage, the library focuses on reconstructing the solenoidal component of a second rank tensor field defined in two-dimensions.

This document provides the mathematical background for the library. A description of the library/script can be found in the [src directory](src/README.md)

## Preliminaries

Some description on the notation used.

$`g \in T^m \R^n`$ reads $`g`$ is a tensor of real numbers of rank $`m`$ defined in $`n`$ dimensions. 

Where $`S`$ is used for the set of symmetric tensors (i.e. $`S \subset T`$). For example  $`f \in S^m \R^n`$.

We adopt a typical mathical description for operator types. For example if an operator (function) $`H`$ takes a symmetric tensor and a vector as input and yields a scalar as an output, this is denotated as:

```math
H : S^2 \R^2 \times S^1\R^2 \to T^0
```

where we refer to vectors as first rank tensors.

The fourier transform of a tensor field $`f(x)`$ is denoted as $`\hat{f}(\omega)`$

## Introduction

According to [Helmholtz Decomposition](https://en.wikipedia.org/wiki/Helmholtz_decomposition), certain tensor fields can be decomposed into curl-free and divergence-free (solenoidal) components. In the notation of V. A. Sharafutdinov, for the field $`f \in S^m \R^n`$:

```math
f = {}^sf + dv
```

where $`{}^sf`$ is the solenoidal component of $`f`$.

The focus on this script is to use the work of V. A. Sharafutdinov to reconstruct $`{}^sf`$ from ray transform measurements of $`f`$.

For pragmatic reasons, we will solely discuss second rank tensors in 2 dimensions.

## Ray Transform (What the experiment records)

The ray transform operator $`I`$ acting on $`f \in S^2 \R^2`$ defined on some domain $`D`$:

```math
I : T^2 \R^2 \times T^1\R^2 \to T^0
```

```math
I(f, x) = \int_{D} <f(x + t\xi), \xi^2>dt
```

Where $`\xi`$ is a unit vector pointing in the direction of the ray and $`<,>`$ is the standard inner product (i.e. $`<f,\xi^2> \equiv (f \cdot \xi) \cdot \xi`$ )

## Back Projection of Tensor Fields

If you are unfamiliar with back projection, I suggest you start [here](https://en.wikipedia.org/wiki/Tomographic_reconstruction).


The back projection operator $`\mu`$ acts on the results of the ray transform. We combine the two in a single operator $`\mu I`$.

```math
\mu I : S^2 \R^2 \times T^1\R^2 \to S^2 \R^2
```

```math
\mu I(f, x) = \frac{1}{\pi} \int_{0}^{\pi} \xi \otimes \xi \int_{D} <f(x + t\xi), \xi^2>dtd\phi
```

where $`\xi = (cos \phi, sin \phi)`$.

For brevity we denote $`h : T^1\R^2 \to S^2\R^2`$ as $`h(x) = \mu I f (x)`$.

## Reconstructing the Solenoidal Component

According to the work published by V. A. Sharafutdinov we can reconstruct the solenoidal component of a symmetric tensor field $`f \in S^2\R^2`$ by the following definition:

```math
{}^s\hat{f}(\omega) = |\omega| \Big( C_0\hat{h}(\omega) + C_1i_{\epsilon}j\hat{h}({\omega}) \Big)
```

where

```math
C_k = (-1)^k\frac{\Gamma(\frac{n-1}{2})}{2\sqrt{\pi}(n-3)!!\Gamma(n/2)}\frac{(n+2m-2k-3)!!}{2^kk!(m-2k)!!}
```

The operator $`!!`$ is defined recursively.

```math
(-1)!! = 1
\newline
(0)!! = 1
\newline
n!! = n(n-2)(n-4)...
```

$`j`$ is the operator of convolution with the generalised Kronecker Tensor ($`\delta \in T^2\R^2`$).

```math
j : S^2\R^2 \to T^0
```

```math
j(f) = \delta * f
```

In Einstein notation
```math
j(f) = \sum_{i=0}^{1}\sum_{j=0}^{1}\delta_{(2-i)(2-j)}f_{(1+i)(j+1)} = f_{21} - f_{12}
```

$`i_\epsilon`$ is defined as:

```math
i_\epsilon = i_\delta - \frac{(i_\omega)^2}{|\omega|^2}
```

Where operator $`i_\delta`$ is symmetric multiplication with $`\delta`$:

```math
i_\delta : S^m\R^n \to S^{m+2}\R^n
```
```math
i_\delta (f) = \sigma (\delta\otimes f)
```
where $`\sigma`$ is the operator of symmetrisation.

Since $`f \in T^0`$ due to operator $`j`$, $`i(f) = f \sigma (\delta) = 0`$ since $`\delta`$ is the an anti-symmetric tensor.

$`(i_\omega)^2`$ is defined as:

```math
(i_\omega)^2 (f) = \sigma (\omega\otimes [\sigma (\omega\otimes f)])
```

Since $`f \in T^0`$ due to operator $`j`$

```math
(i_\omega)^2 (f) = f \omega \otimes \omega
```

Putting it all together:

```math
{}^s\hat{f}(\omega) = |\omega| \Big( C_0\hat{h}(\omega) - C_1\frac{\omega \otimes \omega}{|\omega|^2}j\hat{h}({\omega}) \Big)
```

where

```math
j\hat{h} = \hat{h}_{21} - \hat{h}_{12}
```

Applying the inverse fourier transform to revert to real space we arrive at our final solution for reconstructing the solenoidal component of a second rank symmetric tensor defined in two dimensions:

```math
{}^s{f}(x) = F^{-1}\Big\{|\omega| \Big( C_0\hat{h}(\omega) - C_1\frac{\omega \otimes \omega}{|\omega|^2}j\hat{h}({\omega}) \Big)\Big\} (x)
```
