import typing as t

import numpy as np
import scipy.integrate as integrate

from types import Scalar, Vector, Tensor, ProjDir, Domain


# Working in 2-dimensions for now
NDIM = 2

# TRANSFORM FUNCTION
def dyadic_product(u, v):
    # dyadic product (tensor product) in two dimensions
    return np.kron(u, v).reshape(NDIM, NDIM)

def ray_transform(f: t.Callable[[Vector],Tensor], x: Vector, proj: ProjDir, d: Domain) -> Scalar:
    """
    Ray transform of tensor field (f) (where f: Vector -> Tensor)
    along a line defined by the point (x) and projection direction (proj)
    with x, y limits defined by domain (d).
    """
    
    # convert f to a function requiring one variable (t)
    def g(t):
        # double 'dot' product of tensor field (f) with projection vector (v)
        return f(x + t*proj) @ proj @ proj
    
    # determine bounds for parameter (t)
    x_min, x_max, y_min, y_max = d # unpack domain
    t_min = max((x_min - x[0])/v[0], (y_min - x[1])/v[1])
    t_max = min((x_max - x[0])/v[0], (y_max - x[1])/v[1])    
    
    # numerical intergration of (g) along the ray
    result, err_est = integrate.quad(g, t_min, t_max)
    
    return result

def back_projection(f: t.Callable[[Vector],Tensor], x: Vector, d: Domain) -> Tensor:
    """
    Back projection operator acting on tensor field (f) (where f: Vector -> Tensor)
    at location (x) with x, y limits defined by domain (d).
    """
    # build integration function of one variable (phi)
    def u(phi: Scalar) -> Tensor:
        proj = np.array([np.cos(phi), np.sin(phi)])
        return dyadic_product(proj, proj) * ray_transform(f, x, proj, d)
    
    # Do the back projection for each component individually
    def back_proj_component_ij(i: Scalar, j: Scalar) -> Scalar:
        result, err_est = integrate.quad(lambda phi: u(phi)[i-1][j-1], 0, np.pi)
    
    return np.array([
        back_proj_component_ij(1,1),
        back_proj_component_ij(1,2),
        back_proj_component_ij(2,1),
        back_proj_component_ij(2,2)]).reshape(NDIM, NDIM)
        
    
        
        
    
        


