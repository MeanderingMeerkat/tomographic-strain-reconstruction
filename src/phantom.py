from types import Vector, Tensor, Domain

import numpy as np

# PHANTOM 

def strain_field(p: Vector) -> Tensor:
    """
    Phantom defined at point (p)
    """
    x, y = p
    #return np.array([[1,1],[1,1]])
    return np.array(
        [[x-y*y, x+y],
         [x+y, x*x-2*y]])


domain = Domain((-1, 1, -1, 1))
        


